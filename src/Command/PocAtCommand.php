<?php

namespace App\Command;

use App\Repository\MailRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'app:schedule-cmd',
    description: 'Add a short description for your command',
)]
class PocAtCommand extends Command
{
    public function __construct(private Filesystem $filesystem, private EntityManagerInterface $entityManager, private MailRepository $mailRepository)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->filesystem->appendToFile('./command-log.txt', 'Hello world at '.time().PHP_EOL);
        $io->success('Message written to public/command-log.txt');

        return Command::SUCCESS;
    }
}
