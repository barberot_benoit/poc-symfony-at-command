<?php

namespace App\Controller;

use App\Service\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController->index',
        ]);
    }

    #[Route('/schedule', name: 'app_schedule')]
    public function schedule(MailService $mailService): Response
    {
        $this->scheduleCommand($mailService);

        return $this->render('home/schedule.html.twig', [
            'controller_name' => 'HomeController->schedule',
        ]);
    }

    private function scheduleCommand(): void
    {
        $process = new Process(['echo', 'cd .. && symfony console app:schedule-cmd', '|', 'at', 'now', '+1', 'minutes']);
        $process->run();

        // Alternative:
        exec('echo "cd .. && symfony console app:schedule-cmd" | at now +1 minutes');
    }
}
